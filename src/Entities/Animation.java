package Entities;

import java.awt.image.*;

public class Animation {
	
	private BufferedImage[] frames;//current action/animation that we're doing will be loaded into this
	private int currentFrame;
	
	private long startTime;
	private long delay;
	
	public Animation() {}//token constructor
	
	public void setFrames(BufferedImage[] images) {
		frames = images;
		if(currentFrame >= frames.length) currentFrame = 0;//check to make sure we're not going out of bounds when we're switching between actions
	}
	
	public void setDelay(long d) {
		delay = d;
	}
	
	public void update() {//check if we actually need to actusally need to go into th enext frame
		
		if(delay == -1) return;//if delay == -1, don't do anything at all, no switching of animations
		
		long elapsed = (System.nanoTime() - startTime) / 1000000;//we nmeed to find out how much time has gone by since the last frame
		if(elapsed > delay) {//if this statement...
			currentFrame++;///...go to the next frame...
			startTime = System.nanoTime();///...and reset the start time
		}
		/////when making the sprites, it checks left to right and then loops back to left
		if(currentFrame == frames.length-1) {//one more check to make sure we're not going out of bounds; -1 to get rid of awkward animation skip/jump
			currentFrame = 0;
		}
		
	}
	
	public BufferedImage getImage() {
		return frames[currentFrame];//just returns the current image that we're on
	}
	
}

