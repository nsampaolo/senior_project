package Entities;

import java.awt.*;
import java.awt.image.*;

import javax.imageio.ImageIO;

import java.io.File;

import Game.GamePanel;
import Game.Sound;
import Level.TileMap;

import java.io.File;

public class Player {

	private double x;
	private double y;
	private double dx;
	private double dy;
	public int playerLocX;
	public int playerLocY;
	//this is so we can set the direction from which the bullets are coming from equal to the direction 
	//that the player is facing; 0=up 1=d 2=l 3=r
	public static int direction = 0; 

	public int hitTimer = 0;
	public boolean hitCheck = false;
	int width;//of the player
	int height;

	private boolean left;
	private boolean right;
	/////for UP AND DOWN
	private boolean up;
	private boolean down;
	//	private boolean jumping;//CHANGE to up and down
	private boolean falling;

	private double moveSpeed;//acceleration
	private double maxSpeed; //should be changed to just 'speed'
	//	private double maxFallingSpeed;
	private double stopSpeed;//friction if not moving left or right, will slow us down
	//	private double jumpStart;//vector, should be in neg y direction
	//	private double gravity;

	private TileMap tileMap;

	/////corners of the player/////
	private boolean topLeft;
	private boolean topRight;
	private boolean bottomLeft;
	private boolean bottomRight;

	private Animation animation;//animation object
	private BufferedImage[] idleSprites;
	private BufferedImage[] idleUpSprites;
	private BufferedImage[] walkingSprites;
	private BufferedImage[] jumpingSprites;
	private BufferedImage[] fallingSprites;
	private BufferedImage[] stillSprites;//could be names 'idleDecipher'; gets set to idelUP if you're facing up or down so that it goes idle in that same direction, or idel if you stop and you're facing left or right

	/////for walking up and down
	private BufferedImage[] walkingDownSprites;
	private BufferedImage[] walkingUpSprites;

	private boolean facingLeft, facingRight, facingUp, facingDown;//this is so we can flip the sprites instead of making 2x as many

	public Player(TileMap tm) {//constructor

		tileMap = tm;

		/////each sprite is 22x22 for Kirby
		width = 22;
		height = 22;

		moveSpeed = 0.6;
		maxSpeed = 4.2;
		stopSpeed = 0.30;

		/////in here we'll be reading in all of the sprites
		try {
			idleSprites = new BufferedImage[1];//there is 1 idle sprites
			idleUpSprites = new BufferedImage[1];
			stillSprites = new BufferedImage[1];
			walkingSprites = new BufferedImage[4];//there are six walking sprites
			walkingUpSprites = new BufferedImage[4];//there are 6 walking up sprites
			walkingDownSprites = new BufferedImage[4];//there are 6 walking down sprites

			/////this is where it actually reads in the sprites and loads it into the variables above
			idleSprites[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Protagonist/No_Weapon/Stationary_Left.png"));
			idleUpSprites[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Protagonist/No_Weapon/Stationary_Up.png"));
			//first we have to read the sheet in
			BufferedImage image = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Protagonist/No_Weapon/Walking_Left.png"));
			for(int i = 0; i < walkingSprites.length-1; i++) {//use a loop to get each individual sprite of the six
				walkingSprites[i] = image.getSubimage(//actually have to read in the sprite
						i * width/* + i*/,
						0,
						width,
						height
						);
			}
			//for the walking up animation
			BufferedImage image2 = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Protagonist/No_Weapon/walkingUP_wNothing.png"));//first we have to read the sheet in
			for(int i = 0; i < walkingUpSprites.length-1; i++) {//use a loop to get each individual sprite of the six
				walkingUpSprites[i] = image2.getSubimage(//actually have to read in the sprite
						i * height/* + i*/,//i * width + i,
						0,
						width,
						height
						);
			}
			//for the walking down animation
			BufferedImage image3 = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Protagonist/No_Weapon/Walking_Down.png"));//first we have to read the sheet in
			for(int i = 0; i < walkingDownSprites.length-1; i++) {//use a loop to get each individual sprite of the six
				walkingDownSprites[i] = image3.getSubimage(//actually have to read in the sprite
						i * height/* + i*/,//i * width + i,
						0,
						width,
						height
						);
			}			
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		animation = new Animation();/////create animation class object
		facingLeft = false;
		facingRight = true;
		facingUp = false;
		facingDown = false;
		stillSprites = idleSprites;
	}

	public void addGun(){
		try {

			/////this is where it actually reads in the sprites and loads it into the variables above
			idleSprites[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Protagonist/With_Weapon/Stationary_Left_wPistol.png"));
			idleUpSprites[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Protagonist/With_Weapon/Stationary_Up_wPistol.png"));
			//first we have to read the sheet in
			BufferedImage image = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Protagonist/With_Weapon/walkingLeft_wPistol.png"));
			for(int i = 0; i < walkingSprites.length-1; i++) {//use a loop to get each individual sprite of the six
				walkingSprites[i] = image.getSubimage(//actually have to read in the sprite
						i * width/* + i*/,
						0,
						width,
						height
						);
			}
			//for the walking up animation
			BufferedImage image2 = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Protagonist/With_Weapon/walkingUP_wPistol.png"));//first we have to read the sheet in
			for(int i = 0; i < walkingUpSprites.length-1; i++) {//use a loop to get each individual sprite of the six
				walkingUpSprites[i] = image2.getSubimage(//actually have to read in the sprite
						i * height/* + i*/,//i * width + i,
						0,
						width,
						height
						);
			}
			//for the walking down animation
			BufferedImage image3 = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Protagonist/With_Weapon/walkingDown_wPistol.png"));//first we have to read the sheet in
			for(int i = 0; i < walkingDownSprites.length-1; i++) {//use a loop to get each individual sprite of the six
				walkingDownSprites[i] = image3.getSubimage(//actually have to read in the sprite
						i * height/* + i*/,//i * width + i,
						0,
						width,
						height
						);
			}			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/////set methods/////
	public void setx(int i) { x = i; }
	public void sety(int i) { y = i; }

	public double getPlayerX() { return x; }
	public double getPlayerY() { return y; }

	public void setDirection(int d) { direction = d; } 
	public int getDirection() { return direction; }

	/////set methods for UPLR
	public void setLeft(boolean b) { left = b; }
	public void setRight(boolean b) { right = b; }
	public void setUp(boolean b) { up = b; }
	public void setDown(boolean b) { down = b; }


	private void calculateCorners(double x, double y) {
		int leftTile = tileMap.getColTile((int) (x - width / 2));
		int rightTile = tileMap.getColTile((int) (x + width / 2) - 1);//-1 to keep from going out of bounds
		int topTile = tileMap.getRowTile((int) (y - height / 2));
		int bottomTile = tileMap.getRowTile((int) (y + height / 2) - 1);
		topLeft = tileMap.isBlocked(topTile, leftTile);
		topRight = tileMap.isBlocked(topTile, rightTile);
		bottomLeft = tileMap.isBlocked(bottomTile, leftTile);
		bottomRight = tileMap.isBlocked(bottomTile, rightTile);
	}

	/////////////////////////////////////////////////////////////////////
	public void killed(Enemy_Cop cop, HUD hud){
		if (cop.dead == false){
			if((this.playerLocX + this.width/2 <= cop.EnemyLocX && this.playerLocX + this.width/2 >= (cop.EnemyLocX - cop.width)) &&//hitting the x while shooting up and down to hit the x range
					(this.playerLocY + this.height/2 >= cop.EnemyLocY && this.playerLocY +
					this.height/2 <= (cop.EnemyLocY + cop.height))//hitting the y while shooting left and right to hit the y range
					){
				System.out.println("hitTimer: " + hitTimer);
				if (hitTimer == 0){
					hitTimer++;
					hud.takeHit(this);
					hitCheck = true;
				}

			}
		}
	}
	public void update() {
		if (hitCheck==true){
			hitTimer++;
			if (hitTimer == 20){
				hitTimer = 0;
				hitCheck = false;
			}
		}
		// determine next position LEFT AND RIGHT
		if(left) {
			dx = -maxSpeed;//
			dy = 0;
		}
		else if(right) {//otherwise if we're going right...
			dx = maxSpeed;
			dy = 0;
		}
		/////for UP AND DOWN
		else if(up) {
			dy = -maxSpeed;//
			dx = 0;
		}
		else if(down) {//otherwise if we're going right...
			dy = maxSpeed;
			dx = 0;

		}
		/////adds friction for skidding effect; CHANGE to take out/////
		else {//otherwise, if we're not moving left or right, you should be coming to a stop
			if(dx > 0) {//if still moving to the right...
				dx = 0;
				dx -= stopSpeed;//add friction
				if(dx < 0) {//if we ever pass 0...
					dx = 0;//set to 0
				}
			}
			else if(dx < 0) {//same for the other side going left...
				dx = 0;
				dx += stopSpeed;
				if(dx > 0) {
					dx = 0;

				}
			}

			/////for up and down/////
			//otherwise, if we're not moving left or right, you should be coming to a stop
			if(dy > 0) {//if still moving to the right...
				dy=0;
				dy -= stopSpeed;//add friction
				if(dy < 0) {//if we ever pass 0...
					dy = 0;//set to 0
				}
			}
			else if(dy < 0) {//same for the other side going left...
				dy = 0;
				dy += stopSpeed;
				if(dy > 0) {
					dy = 0;
				}
			}
		}
		/////check collisions

		/////current tile that the player is on/////
		int currCol = tileMap.getColTile((int) x);	//(int) casts them to ints
		int currRow = tileMap.getRowTile((int) y);

		//		
		//our next destination (x and y)
		//position where we should be after updating the x and y
		double tox = x + dx;
		double toy = y + dy;
		//		
		//because we don't want change the actual x and y until the end of the update 
		double tempx = x;
		double tempy = y;

		calculateCorners(x, toy);//calculates x and the destination y bleep bloop bleep.......................................................................
		//		/////all the y collision detection/////
		if(dy < 0) {//if moving upwards...
			if(topLeft || topRight) {//check both top corners; if either one is blocked...
				dy = 0;//stop moving
				tempy = currRow * tileMap.getTileSize() + height / 2;//we want to bump our head into whatever we move into
			}
			else {
				tempy += dy;//else, free to move in up direction
			}
		}
		if(dy > 0) {
			if(bottomLeft || bottomRight) {
				dy = 0;
				falling = false;
				tempy = (currRow + 1) * tileMap.getTileSize() - height / 2;
			}
			else {
				tempy += dy;
			}
		}
		////////https://www.youtube.com/watch?v=x0EhVAlJ2rM&index=3&list=PL-2t7SM0vDffMN9LrFeHtwULZs34pJClv @6:20
		calculateCorners(tox, y);
		if(dx < 0) {//if we're going to the left...
			if(topLeft || bottomLeft) {//check left side corners; if we've hit these corners...
				dx = 0;//stop moving and...
				tempx = currCol * tileMap.getTileSize() + width / 2;//set tempx to stop at right side of block
			}
			else {
				tempx += dx;
			}
		}
		if(dx > 0) {
			if(topRight || bottomRight) {
				dx = 0;
				tempx = (currCol + 1) * tileMap.getTileSize() - width / 2;//because we want to end up on the left side of this block
			}
			else {//otherwise free to move in the x direction
				tempx += dx;
			}
		}

		x = tempx;
		y = tempy;

		/////move the map so that it keep the player in the center of the screen/////
		/////we should look into getting a different camera implemented so that it's not ALWAYS in the middle/////
		tileMap.setx((int) (GamePanel.WIDTH / 2 - x));//minus the player's x
		tileMap.sety((int) (GamePanel.HEIGHT / 2 - y));

		// sprite animation
		if(left || right) {//if left or right keys are pressed...
			animation.setFrames(walkingSprites);
			animation.setDelay(100);//100 milliseconds
			stillSprites = idleSprites;
		}
		else if(up || down){
			animation.setFrames(walkingUpSprites);
			animation.setDelay(100);//100 milliseconds
			stillSprites = idleUpSprites;
		}
		else {//else if left or right keys or not pressed...
			animation.setFrames(stillSprites/*idleSprites*/);
			animation.setDelay(-1);//no animation necessary
		}
		animation.update();
		//the way that the player is facing; 0=up 1=d 2=l 3=r
		if(dy < 0) {
			//animation.setFrames(jumpingSprites);
			//animation.setDelay(-1);
			facingLeft = false;
			facingRight = false;
			facingUp = true;
			facingDown = false;
			direction = 0;
		}
		if(dy > 0) {
			//animation.setFrames(fallingSprites);
			//animation.setDelay(-1);
			facingLeft = false;
			facingRight = false;
			facingUp = false;
			facingDown = true;
			direction = 1;
		}
		//update animation class

		if(dx < 0) {
			facingLeft = true;
			facingRight = false;
			facingUp = false;
			facingDown = false;
			direction = 2;
		}
		if(dx > 0) {
			facingRight = true;
			facingLeft = false;
			facingUp = false;
			facingDown = false;
			direction = 3;
		}

	}

	public int getPlayerLocX() {
		int tx = tileMap.getx();
		return (int) (tx + x - width / 2);
	}

	public int getPlayerLocY() {
		int ty = tileMap.gety();
		return (int) (ty + y - height / 2);
	}

	public void draw(Graphics2D g) {
		//x and y of the tile m ap
		int tx = tileMap.getx();
		int ty = tileMap.gety();

		/////this was for the red box testing
		//g.setColor(Color.RED);
		//g.fillRect((int) (tx+x-width/2), (int) (ty+y-height/2), width, height);

		if(facingLeft) {//check if facgin left or not
			playerLocX=(int) (tx + x - width / 2);
			playerLocY=(int) (ty + y - height / 2);
			g.drawImage(//draw default image
					animation.getImage(),
					playerLocX,
					playerLocY,
					null
					);
		}
		else if(facingRight) {//otherwise if we're facing the right...
			playerLocX=(int) (tx + x - width / 2 + width);
			playerLocY=(int) (ty + y - height / 2);
			g.drawImage(
					animation.getImage(),
					playerLocX,//because when you flip it's like flipping over a mirror and you don't want that because the xoffset gets screwed up
					playerLocY,
					-width,
					height,
					null
					);
		}
		else if(facingUp){
			playerLocX=(int) (tx + x - width / 2);
			playerLocY=(int) (ty + y - height / 2);
			g.drawImage(
					animation.getImage(), 
					playerLocX, 
					playerLocY, 
					null
					);
		}
		else if(facingDown){
			playerLocX=(int) (tx + x - width / 2);
			playerLocY=(int) (ty + y - height / 2 + height);
			g.drawImage(
					animation.getImage(), 
					playerLocX, 
					playerLocY, 
					width,
					-height,
					null
					);
		}
	}

}












