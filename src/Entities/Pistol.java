package Entities;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import Level.TileMap;

public class Pistol {

	private double x;
	private double y;
	
	//private int whichWeapon;
	
	private int width;//of the heart
	private int height;
	private TileMap tileMap;
	public boolean visible = true;
	private int LocX,LocY;
	//private Animation animation;//animation object
	
	private BufferedImage[] pistolGfx;
	
	public Pistol(TileMap tm) {		
		/////sprite heights/////
		width = 14;
		height = 7;
	
		tileMap = tm;
	
		try {
			pistolGfx = new BufferedImage[1];//there is 1 idle sprites
			pistolGfx[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Weaponry/Weapons/Pistol.png"));
			//pistolGfx[1] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Items/HealthBar1.png"));
			//pistolGfx[2] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Items/HealthBar2.png"));
			//pistolGfx[3] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Items/HealthBar3.png"));
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		//animation = new Animation();//create animation class object
	}
	
	public void setx(int i) { x = i; }
	public void sety(int i) { y = i; }
	//public double getPlayerX() { return x; }
	//public double getPlayerY() { return y; }
	
	public void checkTouch(Player P){
		if(((this.LocX - this.width/2 <= P.playerLocX && this.LocX - this.width/2 >= (P.playerLocX - P.width)) &&//hitting the x while shooting up and down to hit the x range
				(this.LocY >= P.playerLocY && this.LocY <= (P.playerLocY + P.height)))
				//hitting the y while shooting left and right to hit the y range
				){
			P.addGun();
			visible = false;
		}
	}
	
	public void draw(Graphics2D g, Player P) {
		if (visible){
			checkTouch(P);
		int tx = tileMap.getx();
		int ty = tileMap.gety();
		LocX = (int) (tx + x - width / 2);
		LocY = (int) (ty + y - height / 2);
		
		g.drawImage(
			//animation.getImage(),
			pistolGfx[0],
			LocX,
			LocY,
			null
			);
		}
	}

}

