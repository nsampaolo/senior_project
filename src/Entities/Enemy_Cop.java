package Entities;

import java.awt.*;
import java.awt.image.*;

import javax.imageio.ImageIO;

import java.io.File;

import Game.GamePanel;
import Game.Sound;
import Level.TileMap;

import java.io.File;

public class Enemy_Cop {

	private double x;
	private double y;
	private double dx;
	private double dy;

	public int EnemyLocX;
	public int EnemyLocY;

	public boolean showCorpse = false;

	public int width;//of the player
	public int height;
	private int currCol, currRow;
	public int deadTimer = 0;
	public int kill2Win = 0;
	

	/////for LEFT, RIGHT, UP, DOWN
	private boolean left;
	private boolean right;
	private boolean up;
	private boolean down;
	private boolean active = false;
	public boolean dead = false;

	private String move;
	private int moveCount = 0;
	private boolean moveRight = true;

	private double maxSpeed;	//should be changed to just 'speed'
	private double stopSpeed;//friction if not moving left or right, will slow us down

	private TileMap tileMap;
	private GamePanel gp;

	/////corners of the player/////
	private boolean topLeft;
	private boolean topRight;
	private boolean bottomLeft;
	private boolean bottomRight;

	private Animation animation;//animation object
	private BufferedImage[] COPidleSprites;
	private BufferedImage[] COPidleUpSprites;
	private BufferedImage[] COPwalkingSprites;
	//private BufferedImage[] COPjumpingSprites;
	//private BufferedImage[] COPfallingSprites;
	private BufferedImage[] COPstillSprites;//could be names 'idleDecipher'; gets set to idelUP if you're facing up or down so that it goes idle in that same direction, or idel if you stop and you're facing left or right
	/////when cop dies it's bloody
	private BufferedImage[] Dead_CopSprites;
	
	/////for walking up and down
	private BufferedImage[] COPwalkingDownSprites;
	private BufferedImage[] COPwalkingUpSprites;
	Sound sound = new Sound();

	private boolean facingLeft, facingRight, facingUp, facingDown;//this is so we can flip the sprites instead of making 2x as many

	public Enemy_Cop(TileMap tm, int startX, int startY, String mover) {//constructor

		move = mover;
		tileMap = tm;

		/////sprite heights/////
		width = 22;//each sprite is 22x22
		height = 22;

		maxSpeed = 1;//2;//3.6;
		stopSpeed = 0.30;

		setx(startX);
		sety(startY);


		/////in here we'll be reading in all of the sprites
		try {		
			COPidleSprites = new BufferedImage[1];//there is 1 idle sprites
			COPidleUpSprites = new BufferedImage[1];
			COPstillSprites = new BufferedImage[1];
			COPwalkingSprites = new BufferedImage[6];//there are six walking sprites
			COPwalkingUpSprites = new BufferedImage[6];//there are 6 walking up sprites
			COPwalkingDownSprites = new BufferedImage[6];//there are 6 walking down sprites
			Dead_CopSprites = new BufferedImage[1];
			
			/////this is where it actually reads in the sprites and loads it into the variables above


			COPidleSprites[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Enemy_Cop/With_Weapon/COPstationary_wPistol.png"));
			COPidleUpSprites[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Enemy_Cop/With_Weapon/COPstationary_Up_wPistol.png"));
			Dead_CopSprites[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Dead_Things/Dead_Cop.png"));
			BufferedImage image = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Enemy_Cop/With_Weapon/COPwalking_wPistol.png"));//first we have to read the sheet in
			for(int i = 0; i < COPwalkingSprites.length-1; i++) {//use a loop to get each individual sprite of the six
				COPwalkingSprites[i] = image.getSubimage(//actually have to read in the sprite
						i * width/* + i*/,
						0,
						width,
						height
						);
			}
			//for the walking up animation
			BufferedImage image2 = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Enemy_Cop/With_Weapon/COPwalking_Up_wPistol.png"));//first we have to read the sheet in
			for(int i = 0; i < COPwalkingUpSprites.length/*-1*/; i++) {//use a loop to get each individual sprite of the six
				COPwalkingUpSprites[i] = image2.getSubimage(//actually have to read in the sprite
						i * width/* + i*/,
						0,
						width,
						height
						);
			}
			//for the walking down animation
			BufferedImage image3 = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Enemy_Cop/With_Weapon/COPwalking_wPistol.png"));//first we have to read the sheet in
			for(int i = 0; i < COPwalkingDownSprites.length/*-1*/; i++) {//use a loop to get each individual sprite of the six
				COPwalkingDownSprites[i] = image3.getSubimage(//actually have to read in the sprite
						i * width/* + i*/,
						0,
						width,
						height
						);
			}	
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		animation = new Animation();//create animation class object
		facingLeft = false;
		facingRight = true;
		facingUp = false;
		facingDown = false;
		COPstillSprites = COPidleSprites;
	}

	/////set methods/////
	public void setx(int i) { x = i; }
	public void sety(int i) { y = i; }
	public void setActive(boolean a) { active = a; }
	public boolean getActive() { return active; }

	/////Set methods for l,r,u,d (directional)
	public void setLeft(boolean b) { left = b; }
	public void setRight(boolean b) { right = b; }
	public void setUp(boolean b) { up = b; }
	public void setDown(boolean b) { down = b; }

	private void calculateCorners(double x, double y) {
		int leftTile = tileMap.getColTile((int) (x - width / 2));
		int rightTile = tileMap.getColTile((int) (x + width / 2) - 1);//-1 to keep from going out of bounds
		//int rightTile = tileMap.getColTile((int) (x + width / 2));
		int topTile = tileMap.getRowTile((int) (y - height / 2));
		int bottomTile = tileMap.getRowTile((int) (y + height / 2) - 1);
		//int bottomTile = tileMap.getRowTile((int) (y + height / 2));
		topLeft = tileMap.isBlocked(topTile, leftTile);
		topRight = tileMap.isBlocked(topTile, rightTile);
		bottomLeft = tileMap.isBlocked(bottomTile, leftTile);
		bottomRight = tileMap.isBlocked(bottomTile, rightTile);
	}

	/////////////////////////////////////////////////////////////////////
	public void line() {
		if (moveRight){
			moveCount++;
			left = false;
			right = true;
			if (moveCount == 50)
				moveRight = false;
		}
		else if (!moveRight){
			moveCount--;
			left = true;
			right = false;
			if (moveCount == 0)
				moveRight = true;
		}
	}
	public void stop(){
		down = up = left = right = false;
	}

	public void follow(Player player) {
		double xDif = player.getPlayerX() - x;
		double yDif = player.getPlayerY() - y;
		//System.out.println(xDif+ " "+yDif);

		if(Math.abs(xDif) < Math.abs(yDif)){
			if(yDif > 0){
				down = true;
				up = false;
				left = false;
				right = false;

			}
			else if(yDif < 0){
				down = false;
				up = true;
				left = false;
				right = false;
			}
		}
		else if(Math.abs(xDif) > Math.abs(yDif)){
			if(xDif < 0){
				left = true;
				right = false;
				up = false;
				down = false;
			}
			else if(xDif > 0){
				left = false;
				right = true;
				up = false;
				down = false;
			}
		}
	}
	public boolean isActive(){
		return active; 
	}
	//bulletLocX is the top left corner pixel of the bullet sprite (it's such a small bullet we can get away with it)
	public void killed(Bullet bullet) {//bullet top right// enemy location is being influenced by direction
		if((bullet.bulletLocX >= this.EnemyLocX && bullet.bulletLocX <= (this.EnemyLocX + this.width)) &&//hitting the x while shooting up and down to hit the x range
				(bullet.bulletLocY >= this.EnemyLocY && bullet.bulletLocY <= (this.EnemyLocY + this.height))//hitting the y while shooting left and right to hit the y range
				)
		{
			if (dead == false)
			bullet.setShoot(false);
			//System.out.println("Killed...");

			//System.out.println("Xbound: ?" + (bullet.bulletLocX >= this.EnemyLocX && bullet.bulletLocX <= (this.EnemyLocX + this.width)));
			//System.out.println("Ybound: ?" + (bullet.bulletLocY <= this.EnemyLocY && bullet.bulletLocY >= (this.EnemyLocY - this.height)));
			//System.out.println("BulletTop <= enemy Top: " + bullet.bulletLocY + "<=" + this.EnemyLocY);
			//System.out.println("BulletTop >= enemy Bottom: " + bullet.bulletLocY + ">=" + (this.EnemyLocY - this.height));
			showCorpse=true;
			dead = true;
			sound.grunt();
			//kill2Win++;
			//System.out.println("kill2Win: " + kill2Win);			
			//System.out.println("showCorpse = " + showCorpse);
		}
		/*System.out.println(bullet.bulletLocX + ">=" + this.EnemyLocX);
			System.out.println(bullet.bulletLocX + "<=" + (this.EnemyLocX + this.width));
			System.out.println(bullet.bulletLocY + "<=" + this.EnemyLocY);
			System.out.println(bullet.bulletLocY + ">=" + (this.EnemyLocY - this.height));*/

	}

	public void set_showCorpse(boolean s){
		showCorpse = s;
	}
	public boolean get_showCorpse() {
		return showCorpse;
	}
	
	
	public void update(Player player) {
		if(kill2Win == 10){
			gp.winMenuSelect = true;
		}
		if(active){
			if (showCorpse){
				stop();
				deadTimer++;
				if(deadTimer > 1000){
					deadTimer = 0;
					active = false;
				}
			}
			else if(move == "line")
			{
				line();
			}

			else if (move == "follow") {
				follow(player);
			}

			// determine next position
			if(left) {
				dx = -maxSpeed;//
				dy = 0;
			}
			else if(right) {//otherwise if we're going right...
				dx = maxSpeed;
				dy = 0;
			}
			/////for UP AND DOWN
			else if(up) {
				dy = -maxSpeed;//
				dx = 0;
			}
			else if(down) {//otherwise if we're going right...
				dy = maxSpeed;
				dx = 0;
			}
			/////adds friction for skidding effect; CHANGE to take out/////
			else {//otherwise, if we're not moving left or right, you should be coming to a stop
				if(dx > 0) {//if still moving to the right...
					dx = 0;
					dx -= stopSpeed;//add friction
					if(dx < 0) {//if we ever pass 0...
						dx = 0;//set to 0
					}
				}
				else if(dx < 0) {//same for the other side going left...
					dx = 0;
					dx += stopSpeed;
					if(dx > 0) {
						dx = 0;

					}
				}

				/////for up and down/////

				//otherwise, if we're not moving left or right, you should be coming to a stop
				if(dy > 0) {//if still moving to the right...
					dy=0;
					dy -= stopSpeed;//add friction
					if(dy < 0) {//if we ever pass 0...
						dy = 0;//set to 0
					}
				}
				else if(dy < 0) {//same for the other side going left...
					dy = 0;
					dy += stopSpeed;
					if(dy > 0) {
						dy = 0;
					}
				}
			}
		}		
		/////check collisions

		/////current tile that the player is on/////
		currCol = tileMap.getColTile((int) x);	//(int) casts them to ints
		currRow = tileMap.getRowTile((int) y);

		//		
		//our next destination (x and y)
		//position where we should be after updating the x and y
		double tox = x + dx;
		double toy = y + dy;
		//		
		//because we don't want change the actual x and y until the end of the update 
		double tempx = x;
		double tempy = y;

		calculateCorners(x, toy);//calculates x and the destination y bleep bloop bleep.......................................................................
		//		/////all the y collision detection/////
		if(dy < 0) {//if moving upwards...
			if(topLeft || topRight) {//check both top corners; if either one is blocked...
				dy = 0;//stop moving
				tempy = currRow * tileMap.getTileSize() + height / 2;//we want to bump our head into whatever we move into
			}
			else {
				tempy += dy;//else, free to move in up direction
			}
		}
		if(dy > 0) {
			if(bottomLeft || bottomRight) {
				dy = 0;
				tempy = (currRow + 1) * tileMap.getTileSize() - height / 2;
			}
			else {
				tempy += dy;
			}
		}
		////////https://www.youtube.com/watch?v=x0EhVAlJ2rM&index=3&list=PL-2t7SM0vDffMN9LrFeHtwULZs34pJClv @6:20
		//calculateCorners(tox, y);
		if(dx < 0) {//if we're going to the left...
			if(topLeft || bottomLeft) {//check left side corners; if we've hit these corners...
				dx = 0;//stop moving and...
				tempx = currCol * tileMap.getTileSize() + width / 2;//set tempx to stop at right side of block
			}
			else {
				tempx += dx;
			}
		}
		if(dx > 0) {
			if(topRight || bottomRight) {
				dx = 0;
				tempx = (currCol + 1) * tileMap.getTileSize() - width / 2;//because we want to end up on the left side of this block
			}
			else {//otherwise free to move in the x direction
				tempx += dx;
			}
		}
		//		
		x = tempx;
		y = tempy;

		/////move the map so that it keep the player in the center of the screen/////
		/////we should look into getting a different camera implemented so that it's not ALWAYS in the middle/////
		tileMap.setx((int) (GamePanel.WIDTH / 2 - x));//minus the player's x
		tileMap.sety((int) (GamePanel.HEIGHT / 2 - y));

		// sprite animation
		if(showCorpse == true){
			//System.out.println("there will be blood...");
			animation.setFrames(Dead_CopSprites/*idleSprites*/);
			animation.setDelay(-1);//no animation necessary
			COPstillSprites = Dead_CopSprites;
		}
		else if(left || right) {//if left or right keys are pressed...
			animation.setFrames(COPwalkingSprites);
			animation.setDelay(100);//100 milliseconds
			COPstillSprites = COPidleSprites;
		}
		else if(up || down){
			animation.setFrames(COPwalkingUpSprites);
			animation.setDelay(100);//100 milliseconds
			COPstillSprites = COPidleUpSprites;
		}
		else {//else if left or right keys or not pressed...
			animation.setFrames(COPstillSprites/*idleSprites*/);
				animation.setDelay(-1);//no animation necessary
			
			
		}
		animation.update();
		if(dy < 0) {
			//animation.setFrames(jumpingSprites);
			//animation.setDelay(-1);
			facingLeft = false;
			facingRight = false;
			facingUp = true;
			facingDown = false;

		}
		if(dy > 0) {
			//animation.setFrames(fallingSprites);
			//animation.setDelay(-1);
			facingLeft = false;
			facingRight = false;
			facingUp = false;
			facingDown = true;

		}
		//update animation class
		if(dx < 0) {
			facingLeft = true;
			facingRight = false;
			facingUp = false;
			facingDown = false;
		}
		if(dx > 0) {
			facingRight = true;
			facingLeft = false;
			facingUp = false;
			facingDown = false;			
		}

	}

	public void draw(Graphics2D g) {
		//x and y of the tile m ap
		int tx = tileMap.getx();
		int ty = tileMap.gety();

		
		//System.out.println((int) (tx + x - width / 2));

		if(facingLeft) {//check if facgin left or not
			EnemyLocX = (int) (tx + x - width / 2);
			EnemyLocY = (int) (ty + y - height / 2);
			g.drawImage(//draw default image
					animation.getImage(),
					EnemyLocX,
					EnemyLocY,
					null
					);
			//EnemyLocX = (int) (tx + x - width / 2);
			//EnemyLocY = (int) (ty + y - height / 2);
			
			/*EnemyLocX = (int) (tx);
			EnemyLocY = (int) (ty);
			EnemyLocX = (int) (x);
			EnemyLocY = (int) (y);*/
		}
		else if(facingRight) {//otherwise if we're facing the right...
			EnemyLocX = (int) (tx + x - width / 2 + width);
			EnemyLocY = (int) (ty + y - height / 2);
			g.drawImage(
					animation.getImage(),
					EnemyLocX,//because when you flip it's like flipping over a mirror and you don't want that because the xoffset gets screwed up
					EnemyLocY,
					-width,
					height,
					null
					);
		}
		else if(facingUp){
			EnemyLocX = (int) (tx + x - width / 2);
			EnemyLocY = (int) (ty + y - height / 2);
			g.drawImage(
					animation.getImage(), 
					EnemyLocX, 
					EnemyLocY, 
					null
					);
		}
		else if(facingDown){
			EnemyLocX = (int) (tx + x - width / 2);
			EnemyLocY = (int) (ty + y - height / 2 + height);
			g.drawImage(
					animation.getImage(), 
					EnemyLocX, 
					EnemyLocY, 
					width,
					-height,
					null
					);
		}
	}

	public int getEnemyLocX() {
		return EnemyLocX;
	}

	public void setEnemyLocX(int enemyLocX) {
		EnemyLocX = enemyLocX;
	}

	public int getEnemyLocY() {
		return EnemyLocY;
	}

	public void setEnemyLocY(int enemyLocY) {
		EnemyLocY = enemyLocY;
	}

}