package Entities;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import Game.Game;
import Game.GamePanel;
import Game.Sound;
import Level.TileMap;
import Entities.Player;

public class Bullet {

	
	private double x;
	private double y;
	private double dx;
	private double dy;
	
	private boolean shoot = false;
	private int startX;
	private int startY;
	private int travelDistanceX = 0, travelDistanceY = 0;
	
	private int width;//of the bullet
	private int height;
	private TileMap tileMap;

	private Animation animation;//animation object
	
	/*private BufferedImage[] bigBlastUP;
	private BufferedImage[] bigBlastDOWN;
	private BufferedImage[] bigBlastLEFT;
	private BufferedImage[] bigBlastRIGHT;*/
	private BufferedImage[] bullet;
	private BufferedImage[] bulletImage;
	public double bulletY, bulletX;
	public int bulletLocX;
	public int bulletLocY;
	public int currCol, currRow;
	public void setx(int i) { x = i; }
	public void sety(int i) { y = i; }
	
	

	public int bulletDirection;
	public boolean visible = false;
	
	public void setShoot(boolean s) { shoot = s; }
	
	public Bullet(TileMap tm, int startX, int startY) {		
		/////sprite heights/////
		width = 7;
		height = 7;
		animation = new Animation();//create animation class object
		tileMap = tm;
		
		try {
			bullet = new BufferedImage[4];//there is 1 idle sprites
			bullet[3] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Weaponry/Bullets/Bullet_Right.png"));
			
			//bigBlastLEFT = new BufferedImage[1];//there is 1 idle sprites
			bullet[2] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Weaponry/Bullets/Bullet_Left.png"));
			
			//bigBlastUP = new BufferedImage[1];//there is 1 idle sprites
			bullet[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Weaponry/Bullets/Bullet_Up.png"));
			
			//bigBlastDOWN  = new BufferedImage[1];//there is 1 idle sprites
			bullet[1] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Weaponry/Bullets/Bullet_Down.png"));
			
			bulletImage = new BufferedImage[1];
			bulletImage[0] = bullet[0];
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		animation = new Animation();//create animation class object
	}
	
	public void setDirection(Player player) {
		bulletDirection = player.getDirection();
		travelDistanceX = 0;
		travelDistanceY = 0;
	}

	public boolean getShoot(){
		return shoot;
	}
	public void bulletMovement(Player player) {
		
		if (bulletDirection == 0){
			travelDistanceY -= 15;
			travelDistanceX = 0;
			bulletImage[0] = bullet[0];
		}
		else if (bulletDirection == 1){
			travelDistanceY += 15;
			travelDistanceX = 0;
			bulletImage[0] = bullet[1];
		}
		else if (bulletDirection == 2){
			travelDistanceY = 0;
			travelDistanceX -= 15;
			bulletImage[0] = bullet[2];
		}
		else if (bulletDirection == 3){
			travelDistanceY = 0;
			travelDistanceX += 15;
			bulletImage[0] = bullet[3];
		}
		if (travelDistanceX > 1000 || travelDistanceY > 1000 || travelDistanceX < -1000 || travelDistanceY < -1000)
			shoot = false;
	}
	
	public void draw(Graphics2D g, Player player) {
		int tx = tileMap.getx();
		int ty = tileMap.gety();
		bulletMovement(player);
		if (shoot){
			//System.out.println(startX + "  " + startY);
			g.drawImage(
				bulletImage[0],//bigBlastUP[0],//animation.getImage()
				/*startX,//player.getPlayerLocX(),//*/
				(int) (tx + x - width / 2) + travelDistanceX,
				/*startY,//player.getPlayerLocY(),//*/
				(int) (ty + y - height / 2) + travelDistanceY,
				null
				);
			bulletLocX = (int) (tx + x - width / 2) + travelDistanceX;
			bulletLocY = (int) (ty + y - height / 2) + travelDistanceY;
			/*bulletLocX = (int) (tx + travelDistanceX);
			bulletLocY = (int) (ty + travelDistanceY);
			bulletLocX = (int) (x);
			bulletLocY = (int) (y);*/
			//System.out.println(tx+ travelDistanceX);
			//System.out.println(ty+ travelDistanceY);
		}
	}
	
	public void update() {
		animation.setFrames(bulletImage);
		animation.update();
	/////move the map so that it keep the player in the center of the screen/////
	/////we should look into getting a different camera implemented so that it's not ALWAYS in the middle/////
		tileMap.setx((int) (GamePanel.WIDTH / 2 - x));//minus the player's x
		tileMap.sety((int) (GamePanel.HEIGHT / 2 - y));
		
		currCol = tileMap.getColTile((int) x);	//(int) casts them to ints
		currRow = tileMap.getRowTile((int) y);
	}
}
