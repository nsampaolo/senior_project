package Entities;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import Game.GamePanel;
import Level.TileMap;

public class HUD {

	private double x;
	private double y;
	private double dx;
	private double dy;
	public int health=0;
	
	private int healthPercentange;
	
	private int width;//of the heart
	private int height;
	private TileMap tileMap;
	private Animation animation;//animation object
	
	private BufferedImage[] heartGfx;
	private BufferedImage[] showHealth;
	private GamePanel gp;
	
	public HUD(TileMap tm, GamePanel GP/*, int xCoor, int yCoor*/) {	
		gp = GP;
		/////sprite heights/////
		width = 22;//each sprite is 22x22
		height = 22;
		animation = new Animation();//create animation class object
	
		tileMap = tm;
	
		try {
			heartGfx = new BufferedImage[3];//there is 1 idle sprites
			showHealth = new BufferedImage[1];
			heartGfx[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Items/Heart/HealthBar0.png"));
			heartGfx[1] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Items/Heart/HealthBar1.png"));
			heartGfx[2] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Items/Heart/HealthBar2.png"));
			showHealth[0] = heartGfx[0];
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		//animation = new Animation();//create animation class object
	}
	
	public void setx(int i) { x = i; }
	public void sety(int i) { y = i; }
	public double getPlayerX() { return x; }
	public double getPlayerY() { return y; }
	
	/*public void howMuchHealth(Player player) {
		
		if (healthPercentange == 0){
			heartGfx[0] = bigBlast[0];
		}
		else if (healthPercentage == 1){

		}
		else if (healthPercentage == 2){
	
		}
		if (travelDistanceX > 1000 || travelDistanceY > 1000)
			shoot = false;
	}*/
	public void takeHit(Player P){
		health++;
		if (health == 1)
			showHealth[0] = heartGfx[1];
		else if (health == 2)
			showHealth[0] = heartGfx[2];
		else if (health == 3){
			//player should die
			//P.hitTimer = 1;
			gp.deadMenuSelect = true;
			gp.MenuSelect = true;
			health = 0;
			showHealth[0] = heartGfx[0];
		}
		System.out.println("health: "+ health);
	}
	
	public void draw(Graphics2D g) {
		int tx = tileMap.getx();
		int ty = tileMap.gety();
		
		g.drawImage(
			//animation.getImage(),
			showHealth[0],
			0,//(int) (tx + x - width / 2),
			0,//(int) (ty + y - height / 2),
			null
			);
	}

}
