package Level;

import java.awt.*;//may not need this
import java.awt.image.*;

public class Tile {
	
	private BufferedImage image;//going to be the tile image that we're going to use
	private boolean blocked;//going to tell us whether or not we can pass through this tile
	
	public Tile(BufferedImage image, boolean blocked) {
		this.image = image;//setting the vars
		this.blocked = blocked;
	}
	/////get methods/////
	public BufferedImage getImage() { return image; }
	public boolean isBlocked() { return blocked; }
	
}