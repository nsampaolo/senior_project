package Level;

import java.io.*;
import java.awt.*;
import java.awt.image.*;//for buffered images
import javax.imageio.ImageIO;

//import Game.GamePanel;

public class TileMap {
	
	private int x;
	private int y;
	
	private int tileSize;
	private int[][] map;
	private int mapWidth;
	private int mapHeight;
	
	/////this will be the tileset gif that will be used
	private BufferedImage tileset;
	////2D array of tiles/////
	private Tile[][] tiles;
	
	/////BLANK BLACK SPACE: to make the camera not show beyond the actual level -> no black space surrounding
//	private int minx;
//	private int miny;
//	private int maxx = 0;
//	private int maxy = 0;
	
	public TileMap(String s, int tileSize) {
		
		this.tileSize = tileSize;
		
		try {
			
			BufferedReader br = new BufferedReader(new FileReader(s));
			
			//parse in the lines that we plan to read
			mapWidth = Integer.parseInt(br.readLine());
			mapHeight = Integer.parseInt(br.readLine());
			map = new int[mapHeight][mapWidth];
			
			
			/////BLANK BLACK SPACE: to make the camera not show beyond the actual level -> no black space surrounding
			//minx = GamePanel.WIDTH - mapWidth * tileSize;
			//miny = GamePanel.HEIGHT - mapHeight * tileSize;
			
			/////for initial testing up to the point of adding tiles/////
			//String delimiters = " ";
			
			/////actual delimeter for implementation/////
			String delimiters = "\\s+";//this means the delimiter will be ANY white space
			
			for(int row = 0; row < mapHeight; row++) {
				String line = br.readLine();//read the line
				String[] tokens = line.split(delimiters);//after reading the line, tokenize it
				for(int col = 0; col < mapWidth; col++) {
					map[row][col] = Integer.parseInt(tokens[col]);
				}
			}
		}
		catch(Exception e) {}
		
	}
	
	 /////this is going to load our tileset into the game/////
	public void loadTiles(String s) {
		
		try {
			
			tileset = ImageIO.read(new File(s));//uses ImageIO classes to read in the images
			
			//since the tiles are arranged in two rows, this will determine how wide it is (tiles across)
			int numTilesAcross = (tileset.getWidth() + 1) / (tileSize + 1);//the +1 is because there is the white/dark purple border that you must take into account on the sprite sheet
			tiles = new Tile[2][numTilesAcross];//new tiles array that reads [2 tiles height][numTileAcross]
			
			/////now we want to get each of the individual tiles
			BufferedImage subimage;
			for(int col = 0; col < numTilesAcross; col++) {
				subimage = tileset.getSubimage(//returns part of the image
					col * tileSize + col,//starting x
					0,//starting y
					/////width and height
					tileSize,
					tileSize
				);
				tiles[0][col] = new Tile(subimage, false);
				subimage = tileset.getSubimage(
					col * tileSize + col,
					tileSize + 1,
					tileSize,
					tileSize
				);//this is all the new subImage...
				tiles[1][col] = new Tile(subimage, false);//now we're setting it in the tiles[][] array; false because it's walkable, it's not blocked
				//tiles[1][col] = new Tile(subimage, true);//now we're setting it in the tiles[][] array
				
				/////retrieve second subImage for the second row
				subimage = tileset.getSubimage(col*tileSize+col, tileSize+1 /*moving down in y val to get bottom half*/, tileSize, tileSize);
				
				/////now create new tile instance
				tiles[1][col] = new Tile(subimage, true);//blocked tile
			}				
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/////get methods/////
	public int getx() { return x; }
	public int gety() { return y; }
	
	public int getColTile(int x) {//find out which tile the player is on
		return x / tileSize;//if at position x, return the current x/the tilesize
	}
	public int getRowTile(int y) {
		return y / tileSize;
	}
	public int getTile(int row, int col) {//returns the value of our map at the row and col; works with calculateCorners
		return map[row][col];
	}
	public int getTileSize() {
		return tileSize;
	}
	
	/////replacing getTile with isBlocked so that we can decipher whether or not there is a block
	public boolean isBlocked(int row, int col) {
		int rc = map[row][col];
		
		/////collectign the row and column of the tile image
		int r = rc / tiles[0].length;
		int c = rc % tiles[0].length;
		return tiles[r][c].isBlocked();
	}
	
	/////set methods/////
	public void setx(int i) {
		x = i;
	
	/////BLANK BLACK SPACE: to make the camera not show beyond the actual level -> no black space surrounding	
	//	if(x < minx) {
	//		x = minx;
	//	}
	//	if(x > maxx) {
	//		x = maxx;
	//	}
	}
	public void sety(int i) {
		y = i;
	/////BLANK BLACK SPACE: to make the camera not show beyond the actual level -> no black space surrounding
	//	if(y < miny) {
	//		y = miny;
	//	}
	//	if(y > maxy) {
	//		y = maxy;
	//	}
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	public void update() {
		
	}
	
	public void draw(Graphics2D g) {
		
		for(int row = 0; row < mapHeight; row++) {
			for(int col = 0; col < mapWidth; col++) {
				
				int rc = map[row][col];//get current value of row and col that we're in
			
				/////to draw from the txt and add colors accordingly to create walkable/non-walkable spaces
			/*	if(rc == 0) {
					g.setColor(Color.BLACK);
				}
				if(rc == 1) {
					g.setColor(Color.WHITE);
				}
				g.fillRect(x + col * tileSize, y + row * tileSize, tileSize, tileSize);
			*/
				int r = rc / tiles[0].length;//row
				int c = rc % tiles[0].length;//column
				
				/////draw the image
				g.drawImage(
					tiles[r][c].getImage(),//getting the image from the tile class we made
					x + col * tileSize,
					y + row * tileSize,
					null
				);	
			}
		}	
	}	
}









