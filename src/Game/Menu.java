package Game;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import Entities.Animation;
import Level.TileMap;

public class Menu {

	private double x;
	private double y;
	private double dx;
	private double dy;
	
	private int healthPercentange;
	
	private int width;//of the heart
	private int height;
	private TileMap tileMap;
	private Animation animation;//animation object
	
	private BufferedImage[] menuScreen;
	private BufferedImage[] continueScreen;
	private BufferedImage[] winScreen;
	
	public Menu(TileMap tm/*, int xCoor, int yCoor*/) {		
		/////sprite heights/////
		width = 1000;//each sprite is 22x22
		height = 600;
		animation = new Animation();//create animation class object
	
		tileMap = tm;
	
		try {
			menuScreen = new BufferedImage[1];//there is 1 idle sprites
			menuScreen[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Items/MenuScreen_Large.png"));
			
			continueScreen = new BufferedImage[1];//there is 1 idle sprites
			continueScreen[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Items/continueScreen.png"));
			
			winScreen = new BufferedImage[1];//there is 1 idle sprites
			winScreen[0] = ImageIO.read(new File(System.getProperty("user.dir")+"/res/Items/WinMenuScreenLarge.png"));
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setx(int i) { x = i; }
	public void sety(int i) { y = i; }
	
	public void draw(Graphics2D g) {
		double tx = x;
		double ty = y;
		
		g.drawImage(
			//animation.getImage(),
			menuScreen[0],
			(int) tx,
			(int) ty,
			null
			);
	}

	public void deadMenuDraw(Graphics2D g) {
		double tx = x;
		double ty = y;
		
		g.drawImage(
			//animation.getImage(),
			continueScreen[0],
			(int) tx,
			(int) ty,
			null
			);		
	}

	public void winMenuDraw(Graphics2D g) {
		double tx = x;
		double ty = y;
		
		g.drawImage(
			//animation.getImage(),
			winScreen[0],
			(int) tx,
			(int) ty,
			null
			);		
	}
	
	
}
