package Game;

import javax.swing.JPanel;

import Entities.Bullet;
import Entities.HUD;
import Entities.Pistol;
import Entities.Player;
import Entities.Enemy_Cop;
import Level.TileMap;
//import PS_Game.gfx.Screen;
//import PS_Game.gfx.SpriteSheet;





import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.util.Random;

public class GamePanel extends JPanel implements Runnable, KeyListener {
	private static final int NUMENEMIES = 50;

	//add KeyListener in order to 'listen' for the key input
	//public class GamePanel extends JPanel implements Runnable {

	public static final int WIDTH = 1000;
	public static final int HEIGHT = 600;

	private Thread thread;
	private boolean running;

	private BufferedImage image;
	private Graphics2D g;
	private Random rand = new Random();

	private int FPS = 30;
	private int targetTime = 1000 / FPS;

	public boolean MenuSelect = true;
	public boolean deadMenuSelect = false;
	public boolean winMenuSelect = false;

	private Menu menu;
	private TileMap tileMap;
	private Player player;
	private Enemy_Cop ec;
	private HUD hud;
	private Pistol pistol;
	private Enemy_Cop[] cop = new Enemy_Cop[100];
	private int copX, copY;
	private Bullet[] bullet = new Bullet[100];//the bullet object is an array of 100
	private int bulletNumber = 0; 
	private int copNumber = 0;
	private int winCounter = 0;
	private int activationCounter = 0;
	private Sound sound = new Sound();

	public GamePanel() {
		super();
		rand.setSeed(1);
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setFocusable(true);
		requestFocus();
	}

	public void addNotify() {
		super.addNotify();
		if(thread == null) {
			thread = new Thread(this);
			thread.start();
		}
		/////in conjunction with KeyListener up top
		addKeyListener(this);
	}
	public int randInt(int min, int max) {

		// Usually this can be a field rather than a method variable
		// Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}	
	public void run() {

		init();

		long startTime;
		long urdTime;
		long waitTime;

		while(running) {
			startTime = System.nanoTime();

			update();
			render();
			draw();

			urdTime = (System.nanoTime() - startTime) / 1000000;
			waitTime = targetTime - urdTime;

			try {
				Thread.sleep(waitTime);
			}
			catch(Exception e) {
			}
		}
	}


	private void init() {

		running = true;

		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) image.getGraphics();

		//tileMap = new TileMap(System.getProperty("user.dir")+"/src/Level/Cell_Level.txt", 32);//tile size of 32
		tileMap = new TileMap(System.getProperty("user.dir")+"/src/Level/Cell_Level.txt", 32);//tile size of 32
		tileMap.loadTiles(System.getProperty("user.dir")+"/res/Tiles/tileset2.png");

		player = new Player(tileMap);
		player.setx(50);	//so the player doesn't start off in the left corner of the screen
		player.sety(50);

		hud = new HUD(tileMap, this);//this passes in the gamepanel...
		hud.setx(50);	//sets the heart location
		hud.sety(50);

		pistol = new Pistol(tileMap);
		pistol.setx(50);	//sets the heart location
		pistol.sety(80);
		//pistol.setx(300);	//sets the heart location
		//pistol.sety(80);

		menu = new Menu(tileMap);
		menu.setx(0);
		menu.sety(0);

		for(int i = 0; i<100; i++){
			copX = randInt(100, 600);
			copY = randInt(200, 400);
			cop[i] = new Enemy_Cop(tileMap,copX,copY, "follow");
			System.out.println("enemy Top: " + cop[i].getEnemyLocY());
			//System.out.println("enemy Bottom: "(this.EnemyLocY - this.height));
			bullet[i] = new Bullet(tileMap, 200, 200);
		}
	}

	//////////////////////////////////////////////////////////////////////////////

	private void update() {
		tileMap.update();

		for(int i = 0; i<100; i++){
			cop[i].update(player);
			bullet[i].update();
		}
		player.update();
		//hud.update();

	}
	private void enemyActivation() {
		if (copNumber <= NUMENEMIES)
			activationCounter++;
		if (activationCounter == 40) {
			activationCounter = 0;
			cop[copNumber].setActive(true);
			cop[copNumber].dead = false;
			if (copNumber <= NUMENEMIES)
				copNumber++;
			//if (copNumber == 100)
			//copNumber = 0;
		}
	}

	private void render() {
		if (deadMenuSelect){
			menu.deadMenuDraw(g);
		}
		else if (winMenuSelect){
			menu.winMenuDraw(g);
		}
		else if (MenuSelect){
			menu.draw(g);
		}
		else{
			/////we're going to need to clear out the screen with black
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, WIDTH, HEIGHT);

			tileMap.draw(g);
			player.draw(g);

			hud.draw(g);
			pistol.draw(g, player);

			enemyActivation();
			/////Generates up to 100 enemies and then deactivates them if they're killed/////
			for(int i = 0; i<100; i++){
				if (cop[i].getActive())
					cop[i].draw(g);
				if (cop[i].dead)
					winCounter++;
				if (winCounter == NUMENEMIES+1)
					winMenuSelect = true;
				if (bullet[i].getShoot())
					bullet[i].draw(g, player);
				for(int j = 0; j<100; j++){// reset to 100
					player.killed(cop[i], hud);
					if(cop[i].isActive() && bullet[j].getShoot()){
						cop[i].killed(bullet[j]);
						/////add to the kill2Win counter
						//ec.kill2Win++;
						//System.out.println("kill2Win: " + ec.kill2Win);
					}
				}
			}
			winCounter = 0;
		}
	}

	private void draw() {
		Graphics g2 = getGraphics();
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
	}

	public void keyTyped(KeyEvent key) {}
	public void keyPressed(KeyEvent key) {

		int code = key.getKeyCode();
		/////I added A,D,W for key input
		if(code == KeyEvent.VK_LEFT || code == KeyEvent.VK_A) {
			player.setLeft(true);
			player.setDirection(2);
		}
		if(code == KeyEvent.VK_RIGHT || code == KeyEvent.VK_D) {
			player.setRight(true);
			player.setDirection(3);
		}
		if(code == KeyEvent.VK_UP || code == KeyEvent.VK_W) {
			player.setUp(true);
			player.setDirection(0);
		}
		if(code == KeyEvent.VK_DOWN || code == KeyEvent.VK_S) {
			player.setDown(true);
			player.setDirection(1);
		}
		if(code == KeyEvent.VK_ENTER) {
			player.hitTimer = 1;
			player.hitCheck = true;
		}
		if(code == KeyEvent.VK_SPACE) {
			if (bulletNumber == 100){
				bulletNumber = 0;
			}
			System.out.println(bulletNumber);
			if (!pistol.visible){
				bullet[bulletNumber].setShoot(true);
				bullet[bulletNumber].setx((int) player.getPlayerX());
				bullet[bulletNumber].sety((int) player.getPlayerY());
				bullet[bulletNumber].setDirection(player);
				bulletNumber++;
				sound.shootSound();
			}

		}

	}
	public void keyReleased(KeyEvent key) {

		int code = key.getKeyCode();

		if(code == KeyEvent.VK_LEFT || code == KeyEvent.VK_A) {
			player.setLeft(false);
		}
		if(code == KeyEvent.VK_RIGHT || code == KeyEvent.VK_D) {
			player.setRight(false);
		}
		/////for UP AND DOWN
		if(code == KeyEvent.VK_UP || code == KeyEvent.VK_W) {
			player.setUp(false);
		}
		if(code == KeyEvent.VK_DOWN || code == KeyEvent.VK_S) {
			player.setDown(false);
		}
		if(code == KeyEvent.VK_ENTER) {

			MenuSelect = false;
			deadMenuSelect = false;
			if (winMenuSelect){
				System.exit(0);
				//Game.main(null);
			}
		}
		if(code == KeyEvent.VK_ESCAPE) {
			MenuSelect = true;
		}
	}

}












